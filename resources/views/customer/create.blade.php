@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Customer Creation Form</div>

                <div class="card-body">                    
                  <customer-customer-component></customer-customer-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


