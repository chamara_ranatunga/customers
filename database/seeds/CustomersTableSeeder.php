<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Customer::class, 5)->create()->each(function ($customer) {
        	$customerAddresses = factory(App\Address::class, 1)->make();
            $customerContacts = factory(App\Contact::class, 3)->make();

            $customer->address()->saveMany($customerAddresses);
            $customer->contact()->saveMany($customerContacts);
    	});
    }
}
