<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Customer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'nic' => $faker->unique->numberBetween(805000000,999999999).'v',        
    ];
});


$factory->define(App\Address::class, function (Faker $faker) {
    return [
        'address' => $faker->streetAddress,
    ];
});


$factory->define(App\Contact::class, function (Faker $faker) {
    return [
        'telephone' => '07'.$faker->unique->numberBetween(10000000,88888888)
    ];
});

