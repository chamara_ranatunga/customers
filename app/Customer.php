<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'name', 'nic',
    ];


    /*
     Getting  address for coustomer

    */	

    public function address()
    {
        return $this->hasOne('App\Address', 'customerId');
    }


    /*
     Getting contacts for coustomer
     
    */	

    public function contact()
    {
        return $this->hasMany('App\Contact','customerId');
    }


}
