<?php

namespace App\Repositories\Contracts;

interface CustomerRepositoryInterface
{
    public function getCustomerList($strSearchText);

    public function getCustomerByid($intId);

    public function insertCustomer($arrData);

    public function updateCustomer($arrdata, $intId);

    public function deleteCustomer($intId);
   
}
