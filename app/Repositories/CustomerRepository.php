<?php

namespace App\Repositories;

use App\Repositories\Contracts\CustomerRepositoryInterface;
use App\Customer;
use App\Address;
use App\Contact;
use DB;

class CustomerRepository extends Repository implements CustomerRepositoryInterface
{

    private $customer;
    private $address;
    private $contact;


    public function __construct(
        Customer $customer,
        Address $address,
        Contact $contact
    )
    {
        $this->customer = $customer;
        $this->address = $address;
        $this->contact = $contact;
    }

    /*
	   *Getting customer data from database
	   *@param $strSearchText
	   *@return array
    */
    public function getCustomerList($strSearchText)
    {

        $customers = $this->customer;
        $customers = $customers::where('customers.name', 'LIKE', '%' . $strSearchText . '%')
            ->leftJoin('addresses', 'customers.id', 'addresses.customerId')
            ->leftJoin('contacts', 'customers.id', 'contacts.customerId')
            ->orWhere('customers.nic', 'LIKE', '%' . $strSearchText . '%')
            ->orWhere('addresses.address', 'LIKE', '%' . $strSearchText . '%')
            ->orWhere('contacts.telephone', 'LIKE', '%' . $strSearchText . '%')
            ->with(['contact', 'address'])
            ->distinct()->get(['customers.*']);
        return $customers;
    }

    /*
       *Getting customer data by id
       *@param $intId
       *@return array
    */
    public function getCustomerByid($intId)
    {

        $customer = $this->customer;
        $customer = $customer::where('customers.id', $intId)
            ->with(['contact', 'address'])->get();
        return $customer;
    }

    /*
       *Insert customer data
       *@param $arrdata
       *@return mix
    */
    public function insertCustomer($arrData)
    {

        $customer = $this->customer;
        $address = $this->address;

        $customer->name = $arrData['name'];
        $customer->nic = $arrData['nic'];
        $customer->save();
        $contactNumbers = [];

        if ($customer->id) {
            if (count($arrData['contacts']) > 0) {
                foreach ($arrData['contacts'] as $key => $value) {
                    $contactNumber = new Contact();
                    $contactNumber->telephone = $value['telephone'];

                    $contactNumbers[] = $contactNumber;
                }
                $customer->contact()->saveMany($contactNumbers);
            }

            $address->address = $arrData['address'];
            $address->customerId = $customer->id;
            $address->save();
            return true;

        } else {
            return false;
        }

    }


    /*
       *update customer data
       *@param $arrdata
       *@param $intId
       *@return mix
    */
    public function updateCustomer($arrData, $intId)
    {

        $customer = $this->customer::where('id', $intId)->first();
        $address = $this->address;

        $customer->name = $arrData['name'];
        $customer->nic = $arrData['nic'];
        $customer->save();

        $this->contact::where('customerId', $intId)->delete();
        $this->address::where('customerId', $intId)->delete();


        if (count($arrData['contacts']) > 0) {
            foreach ($arrData['contacts'] as $key => $value) {
                $contactNumber = new Contact();
                $contactNumber->telephone = $value['telephone'];
                $contactNumbers[] = $contactNumber;
            }
            $customer->contact()->saveMany($contactNumbers);
        }

        $customer->contact()->saveMany($contactNumbers);
        $address->address = $arrData['address'];
        $address->customerId = $customer->id;
        $address->save();

        return true;
    }


    /*
       *delete customer data
       *@param $intId
       *@return mix
    */
    public function deleteCustomer($intId)
    {

        $this->contact::where('customerId', $intId)->delete();
        $this->address::where('customerId', $intId)->delete();
        $this->customer::where('id', $intId)->delete();
        return true;

    }


}