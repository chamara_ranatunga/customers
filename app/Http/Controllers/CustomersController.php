<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Repositories\Contracts\CustomerRepositoryInterface;

class CustomersController extends Controller
{
 	private $customerRepo;
    
    public function __construct(CustomerRepositoryInterface $customerRepo)
    {
        $this->customerRepo = $customerRepo;       
    }


    /*
     *Getting customer details using search text

     *@return array
    */
    public function getCustomerListApi(){

    	try{ 
    		$strSearchText = Input::get('searchText');    		
    	    $arrCustomers = $this->customerRepo->getCustomerList($strSearchText);
            return json_decode($arrCustomers);
        } catch (\Exception $ex) {        	
            return json_decode($ex);
        }

    }


    /*
     *Getting customer details using customer id
     *@param $intId
     *@return array
    */
    public function getCustomerByidApi($intId){

    	try{ 
    		$arrCustomer = $this->customerRepo->getCustomerByid($intId);
    		 return json_decode($arrCustomer);
        } catch (\Exception $ex) { 
            return json_decode($ex);
        }

    }


    /*
     *Getting customer details using search text
     *@return array
    */
    public function postInsertCustomerApi(){

    	try{ 
    		$arrInput = Input::get();    		
    		$respond = $this->customerRepo->insertCustomer($arrInput);	    		
	    	return json_decode($respond);	    		
        } catch (\Exception $ex) {
            return json_decode($ex);
        }

    }


    /*
     *Getting customer details using search text
     *@return array
    */
    public function updateCustomerApi($intId){

    	try{ 
    		$arrInput = Input::get();    		
    		$respond = $this->customerRepo->updateCustomer($arrInput, $intId);	    		
	    	return json_decode($respond);	    		
        } catch (\Exception $ex) { 
            return json_decode($ex);
        }

    }



    /*
     *Getting customer details using search text
     *@return array
    */
    public function deleteCustomerApi($intId){

    	try{	 		
    		$respond = $this->customerRepo->deleteCustomer($intId);	    		
	    	return json_decode($respond);	    		
        } catch (\Exception $ex) {     
           return json_decode($ex);
        }

    }   



}
