<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'telephone',
    ];


    public function customer()
    {
        return $this->belongsTo('App\Customer', 'customerId');
    }
}
