## Clone a repository

Use these steps to clone from SourceTree or command-line. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.


**get the project**
  URL: git clone https://chamara_ranatunga@bitbucket.org/chamara_ranatunga/customers.git
  
# Change directory
  cd customers 
# Install Composer dependencies
  composer install
  
# Copy .env.example to .env
  cp .env.example .env
  
# Generate application secure key (in .env file)
  php artisan key:generate
  
** Create a database (with mysql or postgresql) **

** And update .env file with database credentials **

** DB_CONNECTION=mysql **

** DB_HOST=127.0.0.1 **

** DB_DATABASE=customer **

** DB_USERNAME=root **

** DB_PASSWORD= **




# Run your migrations
  php artisan migrate

# Run your seed
  php artisan db:seed


# Install npm
  npm install
  
# Install vee-validate
  npm install vee-validate --save
  
  npm run dev
  
# Run application 
  php artisan serve



 