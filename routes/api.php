<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/customers', 'CustomersController@getCustomerListApi');
Route::get('/customer/{id}', 'CustomersController@getCustomerByidApi');
Route::post('/customer/create', 'CustomersController@postInsertCustomerApi');
Route::put('/customer/edit/{id}', 'CustomersController@updateCustomerApi');
Route::delete('/customer/delete/{id}', 'CustomersController@deleteCustomerApi');
